# Convaddr

This is a very small utility to help mass-subscribe colleagues from group bulk mail to a mailing list, like Mailman. :-) It basically formats the whole Cc: field into an address-per-line, ready to copypaste into Mailman's text field for mass-subscription.

Requires PyQT5